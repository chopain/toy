json.array!(@mahjongs) do |mahjong|
  json.extract! mahjong, :id, :content, :user_id
  json.url mahjong_url(mahjong, format: :json)
end
