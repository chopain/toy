class MahjongsController < ApplicationController
  before_action :set_mahjong, only: [:show, :edit, :update, :destroy]

  # GET /mahjongs
  # GET /mahjongs.json
  def index
    @mahjongs = Mahjong.all
  end

  # GET /mahjongs/1
  # GET /mahjongs/1.json
  def show
  end

  # GET /mahjongs/new
  def new
    @mahjong = Mahjong.new
  end

  # GET /mahjongs/1/edit
  def edit
  end

  # POST /mahjongs
  # POST /mahjongs.json
  def create
    @mahjong = Mahjong.new(mahjong_params)

    respond_to do |format|
      if @mahjong.save
        format.html { redirect_to @mahjong, notice: 'Mahjong was successfully created.' }
        format.json { render :show, status: :created, location: @mahjong }
      else
        format.html { render :new }
        format.json { render json: @mahjong.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mahjongs/1
  # PATCH/PUT /mahjongs/1.json
  def update
    respond_to do |format|
      if @mahjong.update(mahjong_params)
        format.html { redirect_to @mahjong, notice: 'Mahjong was successfully updated.' }
        format.json { render :show, status: :ok, location: @mahjong }
      else
        format.html { render :edit }
        format.json { render json: @mahjong.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mahjongs/1
  # DELETE /mahjongs/1.json
  def destroy
    @mahjong.destroy
    respond_to do |format|
      format.html { redirect_to mahjongs_url, notice: 'Mahjong was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mahjong
      @mahjong = Mahjong.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mahjong_params
      params.require(:mahjong).permit(:content, :user_id)
    end
end
