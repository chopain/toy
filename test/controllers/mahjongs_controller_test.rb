require 'test_helper'

class MahjongsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @mahjong = mahjongs(:one)
  end

  test "should get index" do
    get mahjongs_url
    assert_response :success
  end

  test "should get new" do
    get new_mahjong_url
    assert_response :success
  end

  test "should create mahjong" do
    assert_difference('Mahjong.count') do
      post mahjongs_url, params: { mahjong: { content: @mahjong.content, user_id: @mahjong.user_id } }
    end

    assert_redirected_to mahjong_url(Mahjong.last)
  end

  test "should show mahjong" do
    get mahjong_url(@mahjong)
    assert_response :success
  end

  test "should get edit" do
    get edit_mahjong_url(@mahjong)
    assert_response :success
  end

  test "should update mahjong" do
    patch mahjong_url(@mahjong), params: { mahjong: { content: @mahjong.content, user_id: @mahjong.user_id } }
    assert_redirected_to mahjong_url(@mahjong)
  end

  test "should destroy mahjong" do
    assert_difference('Mahjong.count', -1) do
      delete mahjong_url(@mahjong)
    end

    assert_redirected_to mahjongs_url
  end
end
